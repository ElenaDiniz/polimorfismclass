//
//  Programador.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Programador: Funcionario {
    
    var plataformas: String
    
    init(nome: String, salario: Float, cpf: Int, plataformas: String) {
        self.plataformas = plataformas
        super.init(nome: nome, salario: salario, cpf: cpf)
    }
    
    override func bonusAnual() -> Float {
        return salario * 0.20
    }
}

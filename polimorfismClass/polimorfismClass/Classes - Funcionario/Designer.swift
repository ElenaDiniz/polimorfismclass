//
//  Designer.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Designer: Funcionario {
    
    var ferramentas: String
    
    init(nome: String, salario: Float, cpf: Int, ferramentas: String) {
        self.ferramentas = ferramentas
        super.init(nome: nome, salario: salario, cpf: cpf)
    }
    
    override func bonusAnual() -> Float {
        return salario * 0.15
    }
}

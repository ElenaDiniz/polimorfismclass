//
//  Funcionario.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Funcionario {
    
    var nome: String
    var salario: Float
    var cpf: Int
    
    init(nome: String, salario: Float, cpf: Int) {
        
        self.nome = nome
        self.salario = salario
        self.cpf = cpf
    }
    
    func bonusAnual() -> Float {
        return salario * 0.10
    }
    
}

//
//  Aviao.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Aviao: Veiculo {
    
    var piloto: String
    var ciaArea: String
    
    init(cor: String, preco: Double, qtdPassageiros: Int, piloto: String, ciaArea: String) {
        self.piloto = piloto
        self.ciaArea = ciaArea
        super.init(cor: cor, preco: preco, qtdPassageiros: qtdPassageiros)
    }
    
    override func distanciaPercorrida(KM: Double) -> Double {
        return Double(qtdPassageiros) * KM
    }
}

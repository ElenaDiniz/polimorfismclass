//
//  Veiculo.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Veiculo {
    
    var cor: String
    var preco: Double
    var qtdPassageiros: Int
    
    init(cor: String, preco: Double, qtdPassageiros:Int) {
        self.cor = cor
        self.preco = preco
        self.qtdPassageiros = qtdPassageiros
    }
    
    func distanciaPercorrida(KM: Double) -> Double {
        return Double(qtdPassageiros) * KM
    }
}

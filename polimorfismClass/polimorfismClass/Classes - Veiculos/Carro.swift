//
//  Carro.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Carro: Veiculo {
    
    var tamanhoRodas: Int
    
    init(cor: String, preco: Double, qtdPassageiros: Int, tamanhoRodas: Int) {
        self.tamanhoRodas = tamanhoRodas
        super.init(cor: cor, preco: preco, qtdPassageiros: qtdPassageiros)
    }
    
    override func distanciaPercorrida(KM: Double) -> Double {
        return Double(tamanhoRodas) * Double(qtdPassageiros) * KM
    }
}

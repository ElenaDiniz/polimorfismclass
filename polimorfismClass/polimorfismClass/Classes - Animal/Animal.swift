//
//  Animal.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Animal {
    
    var nome: String
    var cor: String
    
    init(nome: String, cor: String) {
        
        self.nome = nome
        self.cor = cor
    }
    
    func emitirSom() -> String {
        return "O animal está emitindo o som!"
    }
    
    func comer() {
        print("O animal está comendo.")
    }
    
    func andar() {
        print("O animal \(nome) andou")
    }
    
    func soltaPelo(soltaPelo: Bool) -> String {
        if soltaPelo == true {
            return "\(nome) solta pelos"
            }
            return "\(nome) não solta pelos"
        }
}

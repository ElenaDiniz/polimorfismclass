//
//  Vaca.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import Foundation

class Vaca: Animal {
    
    var litrosDeLeitePorDia: Int
    
    init(nome: String, cor: String, litrosDeLeitePorDia: Int) {
        self.litrosDeLeitePorDia = litrosDeLeitePorDia
        super.init(nome: nome, cor: cor)
    }
    
    override func emitirSom() -> String {
        return "Muuuuuuuuuuuuu"
    }
    
    override func comer() {
        print("Hmmmm. Que capim gostoso!")
    }
    
}

//
//  ViewController.swift
//  polimorfismClass
//
//  Created by Elena Diniz on 08/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    let mimosa:Animal = Vaca(nome: "Mimosa", cor: "Malhada", litrosDeLeitePorDia: 5)
    let tonico:Animal = Gato(nome: "Tonico", cor: "Siamês")
    
    
    let programador: Funcionario = Programador(nome: "João", salario: 5000.00, cpf: 20040030069, plataformas: "iOS")
    let designer: Funcionario = Designer(nome: "Mariana", salario: 3500.00, cpf: 30004045089, ferramentas: "Photoshop")
    
    let carro: Veiculo = Carro(cor: "Preto", preco: 65000.00, qtdPassageiros: 4, tamanhoRodas: 15)
    let aviao: Veiculo = Aviao(cor: "Branco", preco: 2000000.00, qtdPassageiros: 200, piloto: "David", ciaArea: "Latam")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("A quantidade de combustivel gasta pelo veiculo carro é de \(carro.distanciaPercorrida(KM: 150))")
        print("A quantidade de combustivel gasta pelo veiculo aviao é de \(aviao.distanciaPercorrida(KM: 2000))")
        
        print("O bônus do programador \(programador.nome) é de R$ \(programador.bonusAnual())")
        print("O bônus do designer \(designer.nome) é de R$ \(designer.bonusAnual())")

        mimosa.comer()
        tonico.comer()

        mimosa.andar()
        tonico.andar()
        
        // Do any additional setup after loading the view.
    }


}

